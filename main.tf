locals {
  website_files = fileset(var.website_root, "**")
  mime_types = jsondecode(file("mime.json"))
}


######################################################################

resource "aws_s3_bucket" "website" {
  bucket = "${var.website_name}.${var.domain_name}"
}

resource "aws_s3_bucket_website_configuration" "website" {
  bucket = aws_s3_bucket.website.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "index.html"
  }
}

resource "aws_s3_bucket_policy" "AllowProxys" {
  bucket = aws_s3_bucket.website.bucket
  policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [
      {
        Action    = "s3:GetObject"
          Condition = {
            IpAddress = {
              "aws:SourceIp" = [
                "2600:70ff:b856::/48",
                "172.103.185.148/32",
                "2400:cb00::/32",
                "2606:4700::/32",
                "2803:f800::/32",
                "2405:b500::/32",
                "2405:8100::/32",
                "2a06:98c0::/29",
                "2c0f:f248::/32",
                "173.245.48.0/20",
                "103.21.244.0/22",
                "103.22.200.0/22",
                "103.31.4.0/22",
                "141.101.64.0/18",
                "108.162.192.0/18",
                "190.93.240.0/20",
                "188.114.96.0/20",
                "197.234.240.0/22",
                "198.41.128.0/17",
                "162.158.0.0/15",
                "172.64.0.0/13",
                "131.0.72.0/22",
                "104.16.0.0/13",
                "104.24.0.0/14",
              ]
            }
          }
          Effect    = "Allow"
          Principal = "*"
          Resource  = "${aws_s3_bucket.website.arn}/*"
          Sid       = "PublicReadGetObject"
      },
      {
        Action    = "s3:GetObject"
        Effect    = "Allow"
        Principal = {
          AWS = aws_cloudfront_origin_access_identity.s3_distribution.iam_arn
        }
        Resource  = "${aws_s3_bucket.website.arn}/*"
        Sid       = "PolicyForCloudFrontPrivateContent"
      },
    ]
  })
}

#resource "aws_s3_object" "sitepublic" {
#  for_each = local.website_files
#  bucket   = aws_s3_bucket.website.bucket
#  key      = each.value
#  source   = "${var.website_root}${each.value}"
#  etag     = filemd5("${var.website_root}${each.value}")  #FIX TODO?
#
#  content_type = lookup(local.mime_types, regex("\\.[^.]+$", each.key), null)
#}


resource "aws_acm_certificate" "acm" {
  provider                  = aws.us-east-1
  domain_name               = "${var.website_name}.${var.domain_name}"
  subject_alternative_names = ["api-${var.website_name}.${var.domain_name}"]
  validation_method = "DNS"

  tags = {
    Environment = var.Environment
  }

  lifecycle {
    create_before_destroy = true
        ignore_changes = [
      status,
    ]
  }
}

resource "cloudflare_record" "acm" {
  for_each = {
    for dvo in aws_acm_certificate.acm.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      #value  = dvo.resource_record_value
      value  = "${replace(dvo.resource_record_value, "/[.]$/", "")}"
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  value           = each.value.value
  type            = each.value.type
  
  zone_id         = var.cloudflare_zone_id
  proxied         = false
}

resource "aws_acm_certificate_validation" "acm" {
  provider                = aws.us-east-1
  certificate_arn         = aws_acm_certificate.acm.arn
  validation_record_fqdns = [for value in cloudflare_record.acm : value.hostname]
}

resource "aws_cloudfront_origin_access_identity" "s3_distribution" {
  comment = "access-identity-${aws_s3_bucket.website.bucket_regional_domain_name}"
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  aliases = ["${var.website_name}.${var.domain_name}"]

  origin {
    domain_name = aws_s3_bucket.website.bucket_regional_domain_name
    origin_id   = aws_s3_bucket.website.bucket_regional_domain_name

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.s3_distribution.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    #allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    compress         = true
    target_origin_id = aws_s3_bucket.website.bucket_regional_domain_name

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0 # DOCS?? 0
    default_ttl            = 0 # DOCS?? 3600
    max_ttl                = 0 # DOCS?? 86400
  }

  price_class = "PriceClass_100"  # Region PriceClass_100 = USA, Canada, Europe, & Israel

  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []

      #restriction_type = "whitelist"
      #locations        = ["US", "CA", "GB", "DE"]
    }
  }

  tags = {
    Environment = var.Environment
  }

  viewer_certificate {
      acm_certificate_arn            = aws_acm_certificate.acm.arn
      cloudfront_default_certificate = false
      minimum_protocol_version       = "TLSv1.2_2021"
      ssl_support_method             = "sni-only"
  }
}

resource "cloudflare_record" "website" {
  allow_overwrite = true
  name            = "${var.website_name}"
  value           = aws_cloudfront_distribution.s3_distribution.domain_name
  type            = "CNAME"
  
  zone_id         = var.cloudflare_zone_id
  proxied         = false
}


resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "siteVisits"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "siteURL"

  attribute {
    name = "siteURL"
    type = "S"
  }

  tags = {
    Name        = "${var.website_name}-table-1"
    Environment = var.Environment
  }
}


resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# See also the following AWS managed policy: AWSLambdaBasicExecutionRole
resource "aws_iam_policy" "lambda_logging_DynamoDB" {
  name        = "lambda_logging_DynamoDB"
  path        = "/"
  description = "IAM policy for logging from a lambda + DynamoDB"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "VisualEditor0",
      "Effect": "Allow",
      "Action": "logs:CreateLogGroup",
      "Resource": "arn:aws:logs:ca-central-1:412349736044:*"
    },
    {
      "Sid": "VisualEditor1",
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:ca-central-1:412349736044:log-group:/aws/lambda/${aws_lambda_function.test_lambda.function_name}:*"
    },
    {
      "Sid": "VisualEditor2",
      "Effect": "Allow",
      "Action": "dynamodb:*",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging_DynamoDB.arn
}

resource "aws_lambda_permission" "lambda_permission" {
  #statement_id  = "AllowMyDemoAPIInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  # The /*/*/* part allows invocation from any stage, method and resource path
  # within API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
}


####################

resource "time_sleep" "wait_10_seconds" {
  create_duration = "10s"
}

data "archive_file" "python_lambda_package" {
  depends_on = [time_sleep.wait_10_seconds]
  type = "zip"
  source_file = "${path.module}/lambda_function.py"
  output_path = "lambda_function_payload.zip"
}

resource "aws_lambda_function" "test_lambda" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  filename      = "lambda_function_payload.zip"
  function_name = "WebSiteCounter"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "lambda_function.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  #source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  #source_code_hash = filebase64sha256("lambda_function_payload.zip")
  source_code_hash = data.archive_file.python_lambda_package.output_base64sha256

  runtime = "python3.9"

  tags = {
    Environment = var.Environment
  }
}

######### API Gateway https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration#lambda-integration
resource "aws_api_gateway_rest_api" "api" {
  name = "WebSiteCounterAPI"
}

resource "aws_api_gateway_resource" "resource" {
  path_part   = "resource"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.resource.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.test_lambda.invoke_arn
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.resource.id
  http_method = aws_api_gateway_method.method.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_deployment" "api" {
  rest_api_id = aws_api_gateway_rest_api.api.id

  triggers = {
    # NOTE: The configuration below will satisfy ordering considerations,
    #       but not pick up all future REST API changes. More advanced patterns
    #       are possible, such as using the filesha1() function against the
    #       Terraform configuration file(s) or removing the .id references to
    #       calculate a hash against whole resources. Be aware that using whole
    #       resources will show a difference after the initial implementation.
    #       It will stabilize to only change when resources change afterwards.
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.resource.id,
      aws_api_gateway_method.method.id,
      aws_api_gateway_integration.integration.id,
      aws_api_gateway_method_response.response_200,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "api" {
  deployment_id = aws_api_gateway_deployment.api.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
  stage_name    = "v1"
}

######## Start API

resource "aws_api_gateway_domain_name" "apiDomainName" {
  depends_on = [aws_acm_certificate_validation.acm]
  certificate_arn = aws_acm_certificate_validation.acm.certificate_arn
  domain_name     = "api-${var.website_name}.${var.domain_name}"
}

resource "aws_api_gateway_base_path_mapping" "apiDomainName" {
  api_id      = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_stage.api.stage_name
  domain_name = aws_api_gateway_domain_name.apiDomainName.domain_name
}

resource "cloudflare_record" "apiwebsite" {
  allow_overwrite = true
  name            = "api-${var.website_name}"
  value           = aws_api_gateway_domain_name.apiDomainName.cloudfront_domain_name
  type            = "CNAME"

  zone_id         = var.cloudflare_zone_id
  proxied         = false
}