import json
import boto3

dynamodb = boto3.client('dynamodb')

def lambda_handler(event, context):
    response = dynamodb.update_item(
        TableName='siteVisits',
        Key={
            'siteURL': {"S": "https://this-is-thomas.tnepeters.com/"}
        },
        UpdateExpression="SET visits = if_not_exists(visits, :start) + :inc",
        ExpressionAttributeValues={
            ':inc': {'N': '1'},
            ':start': {'N': '0'},
        },
        ReturnValues="UPDATED_NEW"
    )
    item = response['Attributes']['visits']['N']
    
    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': 'https://this-is-thomas.tnepeters.com',
            'Access-Control-Allow-Methods': 'GET'
            #'Access-Control-Allow-Origin': 'http://localhost:3000',
            #'Access-Control-Allow-Origin': '*',
            #'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        'body': json.dumps(str(item))
    }
