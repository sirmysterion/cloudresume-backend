variable "cloudflare_email" {
  default = ""
  type    = string
}

variable "cloudflare_api_key" {
  default = ""
  type    = string
}

variable "cloudflare_zone_id" {
  default = ""
  type    = string
}

variable "website_root" {
  default = "html/"
  type    = string
}

variable "website_name" {
  default = ""
  type    = string
}

variable "domain_name" {
  default = ""
  type    = string
}

variable "aws_profile" {
  default = ""
  type    = string
}

variable "aws_region" {
  default = "ca-central-1"
  type    = string
}

variable "Environment" {
  default = "Production"
  type    = string
}