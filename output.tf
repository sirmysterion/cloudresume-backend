output "Aliase" {
  value = aws_cloudfront_distribution.s3_distribution.aliases
}

output "webURL" {
  value = "http://${aws_cloudfront_distribution.s3_distribution.domain_name}"
}

output "BucketURL" {
  #value = "http://${aws_s3_bucket.website.website_endpoint}"
  value = "http://${aws_s3_bucket.website.website_endpoint}"  == null ? "Pending Re-run" : "http://${aws_s3_bucket.website.website_endpoint}"
}