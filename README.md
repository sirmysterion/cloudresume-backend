# Getting Started

### Setting up Terraform State File
```
export GITLAB_ACCESS_TOKEN=<YOUR-ACCESS-TOKEN>
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/35462803/terraform/state/gitlab-state" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/35462803/terraform/state/gitlab-state/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/35462803/terraform/state/gitlab-state/lock" \
    -backend-config="username=sirmysterion" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```